/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef QUICK_ANA__X_AOD_EVENT_INFO_H
#define QUICK_ANA__X_AOD_EVENT_INFO_H

//        
//                  Author: Nils Krumnack
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

// Please feel free to contact me (nils.erik.krumnack@cern.ch) for bug
// reports, feature suggestions, praise and complaints.



#include <QuickAna/Global.h>

#include <xAODEventInfo/EventInfo.h>

namespace ana
{
  typedef xAOD::EventInfo EventSelectType;
}

#endif
